import React, { useContext } from "react";
import { Radio } from "antd";
import { StoreContext } from "../Context/StoreContext";
import * as constants from "../Context/constants";

const Filter = () => {
  const { actions } = useContext(StoreContext);

  const handleFilter = filter => {
    actions.filterTodo(filter);
  };
  return (
    <Radio.Group
      defaultValue={constants.FILLTER_ALL}
      onChange={e => handleFilter(e.target.value)}
    >
      <Radio.Button value={constants.FILLTER_ALL}>ALL</Radio.Button>
      <Radio.Button value={constants.FILLTER_NEW}>New</Radio.Button>
      <Radio.Button value={constants.FILLTER_DONE}>Done</Radio.Button>
    </Radio.Group>
  );
};
export default Filter;
