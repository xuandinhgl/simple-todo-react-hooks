import React, { useContext } from "react";
import { List, Icon } from "antd";
import { StoreContext } from "../Context/StoreContext";
import * as constants from "../Context/constants";

function TodoList() {
  const { state, actions } = useContext(StoreContext);
  const toggleStatus = id => {
    actions.toggleStatus(id);
  };
  const toggleEdit = id => {
    actions.toggleEdit(id);
  };

  let filterTodo = [];
  if (state.filter === constants.FILLTER_DONE) {
    filterTodo = state.todos.filter(todo => todo.status === true);
  } else if (state.filter === constants.FILLTER_NEW) {
    filterTodo = state.todos.filter(todo => todo.status === false);
  } else {
    filterTodo = state.todos;
  }

  return (
    <List
      dataSource={filterTodo}
      renderItem={item => (
        <List.Item
          actions={[
            <span
              style={{ color: "#40a9ff" }}
              onClick={() => {
                toggleEdit(item.id);
              }}
            >
              <Icon type="edit" />
            </span>,
            <span
              style={{ color: "#40a9ff" }}
              onClick={() => {
                toggleStatus(item.id);
              }}
            >
              <Icon type="check" />
            </span>
          ]}
        >
          <div
            style={{ textDecoration: item.status ? "line-through" : "none" }}
          >
            {item.text}
          </div>
        </List.Item>
      )}
    />
  );
}
export default TodoList;
