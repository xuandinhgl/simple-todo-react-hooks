import React, { useContext, useState, useEffect } from "react";
import { Row, Col, Input, Button, message } from "antd";
import { StoreContext } from "../Context/StoreContext";
import { generateID } from "../helpers";

function Form() {
  const { state, actions } = useContext(StoreContext);
  const [todo, setTodo] = useState("");

  useEffect(() => {
    if (state.isEdit) {
      const editItem = state.todos.filter(todo => todo.id === state.isEdit);
      setTodo(editItem[0].text);
    }
  }, [state.isEdit, state.todos]);

  const handleChange = e => {
    setTodo(e.target.value);
  };

  const handleSubmit = () => {
    if (todo !== "") {
      if (state.isEdit) {
        const oldTodo = state.todos.filter(todo => todo.id === state.isEdit);
        oldTodo.text = todo;
        actions.updateTodo(todo);
        actions.toggleEdit(0);
        message.info("To do is updated", 1);
      } else {
        const newTodo = {
          id: generateID(7),
          status: false,
          text: todo
        };
        actions.addTodo(newTodo);
        message.info("Todo added", 1);
      }
      setTodo("");
    }
  };

  return (
    <div>
      <Row style={{ marginBottom: 20 }} gutter={4}>
        <Col span={20}>
          <Input onChange={handleChange} value={todo} />
        </Col>
        <Col span={4} style={{ textAlign: "center" }}>
          <Button type="primary" icon="plus" onClick={handleSubmit}>
            {state.isEdit ? "Update" : "Add"}
          </Button>
        </Col>
      </Row>
    </div>
  );
}

export default Form;
