import * as constants from "./constants";

export const useActions = dispatch => {
  const addTodo = todo => {
    dispatch({
      type: constants.ADD_TODO,
      payload: todo
    });
  };

  const toggleEdit = id => {
    dispatch({
      type: constants.TOGGLE_EDIT,
      payload: id
    });
  };

  const filterTodo = filter => {
    dispatch({
      type: constants.FILTER_TODO,
      payload: filter
    });
  };

  const toggleStatus = id => {
    dispatch({
      type: constants.TOGGLE_STATUS,
      payload: id
    });
  };

  const updateTodo = todo => {
    dispatch({
      type: constants.UPDATE_TODO,
      payload: todo
    });
  };
  return {
    addTodo,
    toggleEdit,
    filterTodo,
    toggleStatus,
    updateTodo
  };
};
