import React, { createContext, useReducer } from "react";
import { useActions } from "./Actions";
import { initialState, reducer } from "./Reducers";

const StoreContext = createContext(initialState);
const StoreProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const actions = useActions(dispatch);
  return (
    <StoreContext.Provider value={{ state, dispatch, actions }}>
      {children}
    </StoreContext.Provider>
  );
};

export { StoreContext, StoreProvider };
