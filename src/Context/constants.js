export const ADD_TODO = "ADD_TODO";
export const UPDATE_TODO = "UPDATE_TODO";
export const FILTER_TODO = "FILTER_TODO";
export const TOGGLE_STATUS = "TOGGLE_STATUS";
export const TOGGLE_EDIT = "TOGGLE_EDIT";
export const FILLTER_ALL = "ALL";
export const FILLTER_NEW = "NEW";
export const FILLTER_DONE = "DONE";
