import * as constants from "./constants";
import { getTodoFromLocalStorage, updateTodoLocalStorage } from "../helpers";

const initialState = {
  isEdit: 0,
  filter: constants.FILLTER_ALL,
  todos: getTodoFromLocalStorage()
};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case constants.ADD_TODO:
      const todos = [...state.todos, payload];
      updateTodoLocalStorage(todos);
      return { ...state, todos };
    case constants.TOGGLE_EDIT:
      const editItem = state.todos.filter(todo => todo.id === payload);
      return {
        ...state,
        isEdit: editItem.length ? editItem[0].id : 0
      };
    case constants.TOGGLE_STATUS:
      const todoItems = state.todos.map(todo =>
        todo.id !== payload ? todo : { ...todo, status: !todo.status }
      );
      updateTodoLocalStorage(todoItems);
      return {
        ...state,
        todos: todoItems
      };
    case constants.FILTER_TODO:
      return { ...state, filter: payload };

    case constants.UPDATE_TODO:
      const todoUpdated = state.todos.map(todo =>
        todo.id === state.isEdit ? { ...todo, text: payload } : todo
      );
      updateTodoLocalStorage(todoUpdated);
      return {
        ...state,
        todos: todoUpdated
      };

    default:
      return state;
  }
};

export { initialState, reducer };
