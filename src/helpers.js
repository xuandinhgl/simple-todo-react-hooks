export const generateID = length => {
  var result = "";
  var characters = "abcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

export const getTodoFromLocalStorage = () => {
  const todoFromLocalStorage = localStorage.getItem("xdiTodos");
  if (todoFromLocalStorage) {
    return JSON.parse(todoFromLocalStorage);
  }
  return [];
};

export const updateTodoLocalStorage = todos => {
  localStorage.setItem("xdiTodos", JSON.stringify(todos));
};
