import React from "react";
import { Typography, Row, Col } from "antd";
import Form from "./Components/Form";
import TodoList from "./Components/TodoList";
import Filter from "./Components/Filter";

const { Title } = Typography;
function App() {
  return (
    <div className="App">
      <Row>
        <Col span={5} offset={9}>
          <Title level={2} style={{ textAlign: "center", paddingTop: "30px" }}>
            Simple Todo App
          </Title>
          <Form />
          <Filter />
          <TodoList />
        </Col>
      </Row>
    </div>
  );
}

export default App;
